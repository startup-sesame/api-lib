const Api = require('./Api');

const setAuthToken = (token) => {
  localStorage.userJwt = token;
  Api.defaults.headers.common.Authorization = `Bearer ${token}`;
};

module.exports = {
  register: (username, name, email, password) =>
    Api.post('auth/local/register', {
      email,
      password,
      username,
      person: {
        name,
      },
    })
      .then((response) => {
        setAuthToken(response.data.jwt);

        return response;
      }),
  setAuthToken,
  login: (email, password) =>
    Api.post('auth/local', {
      identifier: email,
      password,
    })
      .then((response) => {
        setAuthToken(response.data.jwt);

        return response;
      }),
  passwordLost: email =>
    Api.post('auth/forgot-password', {
      email,
    }),
  resetPassword: (code, password, passwordConfirmation) =>
    Api.post('auth/reset-password', {
      code,
      password,
      passwordConfirmation,
    }),
  isAuthenticated: () => !!localStorage.userJwt,
  logout: () => {
    localStorage.userJwt = '';
    Api.defaults.headers.common.Authorization = '';
  },
};
