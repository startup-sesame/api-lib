const axios = require('axios');
const delay = ms => new Promise(_ => setTimeout(_, ms));

const Api = axios.create({
  baseURL: process.env.SESAMERS_API_URL || 'https://api.sesamers.com',
  timeout: 4000,
});

if (typeof localStorage !== 'undefined' && localStorage && localStorage.userJwt) {
  Api.defaults.headers.common.Authorization = `Bearer ${localStorage.userJwt}`;
}


Api.interceptors.response.use((response) => {
  return response;
}, async (error) => {
  if (error.config && error.response && error.response.status === 502) {
    await delay(300);
    return axios.request(error.config);
  }
  
  return new Promise((resolve, reject) => {
    reject(error);
  });
});

module.exports = Api;
