const BaseEntity = require('./BaseEntity');

class CompanyRolesApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'companyroles';
  }
}

module.exports = new CompanyRolesApi();
