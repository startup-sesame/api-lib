const BaseEntity = require('./BaseEntity');

class CompaniesApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'companies';
  }
}

module.exports = new CompaniesApi();
