const BaseEntity = require('./BaseEntity');

class IntrosApi extends BaseEntity {
    constructor(...props) {
        super(...props);

        this.slug = 'intros';
    }
}

module.exports = new IntrosApi();
