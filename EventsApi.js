const momentTZ = require('moment-timezone');
const BaseEntity = require('./BaseEntity');
const Api = require('./Api');

class EventsApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'events';
  }

  cleanDates(entity) {
    const newEntity = super.cleanDates(entity);

    if (newEntity.start_datetime) {
      newEntity.start_datetime = momentTZ(newEntity.start_datetime).format('YYYY-MM-DD HH:mmZZ');
    }

    if (newEntity.end_datetime) {
      newEntity.end_datetime = momentTZ(newEntity.end_datetime).format('YYYY-MM-DD HH:mmZZ');
    }

    return newEntity;
  }

  save(entity) {
    const dataToSave = Object.assign({}, entity);

    dataToSave.start_datetime = momentTZ.utc(dataToSave.start_datetime).format('YYYY-MM-DD HH:mm');
    dataToSave.end_datetime = momentTZ.utc(dataToSave.end_datetime).format('YYYY-MM-DD HH:mm');

    if (dataToSave.location) {
      dataToSave.location = JSON.parse(JSON.stringify(dataToSave.location).replace(/[\u0800-\uFFFF]/g, ''));
    }

    return super.save(dataToSave);
  }

  getParticipatedPeople(id) {
    return Api.get(`${this.slug}/${id}/list-people`).then((results) => {
      const newResults = Object.assign({}, results);
      newResults.data = [];

      results.data.forEach((result) => {
        newResults.data.push(this.cleanDates(result));
      });

      return newResults;
    });
  }
}

module.exports = new EventsApi();
