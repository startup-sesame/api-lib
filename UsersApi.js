const Api = require('./Api');
const BaseEntity = require('./BaseEntity');

class UsersApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'users';
  }

  me() {
    return Api.get('users/me')
  }
}

module.exports = new UsersApi();
