const BaseEntity = require('./BaseEntity');

class ClassificationsApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'classifications';
  }
}

module.exports = new ClassificationsApi();
