const momentTZ = require('moment-timezone');
const BaseEntity = require('./BaseEntity');

class StartupStandsApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'startupstands';
  }

  cleanDates(entity) {
    const newEntity = super.cleanDates(entity);

    if (newEntity.start_datetime) {
      newEntity.start_datetime = momentTZ(newEntity.start_datetime).format('YYYY-MM-DD HH:mmZZ');
    }

    if (newEntity.end_datetime) {
      newEntity.end_datetime = momentTZ(newEntity.end_datetime).format('YYYY-MM-DD HH:mmZZ');
    }

    return newEntity;
  }

  save(entity) {
    const dataToSave = Object.assign({}, entity);

    dataToSave.start_datetime = momentTZ.utc(dataToSave.start_datetime).format('YYYY-MM-DD HH:mm');
    dataToSave.end_datetime = momentTZ.utc(dataToSave.end_datetime).format('YYYY-MM-DD HH:mm');

    return super.save(dataToSave);
  }
}

module.exports = new StartupStandsApi();
