const BaseEntity = require('./BaseEntity');

class CompanyMembersApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'companymembers';
  }
}

module.exports = new CompanyMembersApi();
