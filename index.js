"use strict";

const Api = require('./Api');
const AuthApi = require('./AuthApi');
const ClassificationsApi = require('./ClassificationsApi');
const CompaniesApi = require('./CompaniesApi');
const CompanyMembersApi = require('./CompanyMembersApi');
const CompanyRolesApi = require('./CompanyRolesApi');
const SessionsApi = require('./SessionsApi');
const EventsApi = require('./EventsApi');
const FundraisingsApi = require('./FundraisingsApi');
const OneToOnesApi = require('./OneToOnesApi');
const PartcompaniesApi = require('./PartcompaniesApi');
const PeopleApi = require('./PeopleApi');
const PrivateEventsApi = require('./PrivateEventsApi');
const SideEventsApi = require('./SideEventsApi');
const StartupStandsApi = require('./StartupStandsApi');
const TagsApi = require('./TagsApi');
const UsersApi = require('./UsersApi');
const IntrosApi = require('./IntrosApi');

module.exports = {
    Api,
    AuthApi,
    ClassificationsApi,
    CompaniesApi,
    CompanyMembersApi,
    CompanyRolesApi,
    SessionsApi,
    EventsApi,
    FundraisingsApi,
    OneToOnesApi,
    PartcompaniesApi,
    PeopleApi,
    PrivateEventsApi,
    SideEventsApi,
    StartupStandsApi,
    TagsApi,
    UsersApi,
    IntrosApi,
};
