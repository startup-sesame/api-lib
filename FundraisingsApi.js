const momentTZ = require('moment-timezone');
const BaseEntity = require('./BaseEntity');

class FundraisingsApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'fundraisings';
  }
}

module.exports = new FundraisingsApi();
