const BaseEntity = require('./BaseEntity');

class PartcompaniesApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'partcompanies';
  }
}

module.exports = new PartcompaniesApi();
