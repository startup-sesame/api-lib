const momentTZ = require('moment-timezone');
const Api = require('./Api');
const EventsApi = require('./EventsApi');
const BaseEntity = require('./BaseEntity');

class PeopleApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'people';
  }

  cleanDates(entity) {
    const newEntity = super.cleanDates(entity);

    if (newEntity.premium_date) {
      newEntity.premium_date = momentTZ(newEntity.premium_date).format('YYYY-MM-DD');
    }

    return newEntity;
  }

  getParticipatedEvents(id) {
    return Api.get(`${this.slug}/${id}/list-events`).then((results) => {
      const newResults = Object.assign({}, results);
      newResults.data = [];

      results.data.forEach((result) => {
        newResults.data.push(EventsApi.cleanDates(result));
      });

      return newResults;
    });
  }
}

module.exports = new PeopleApi();
